<?php
session_start();

$logged = $_SESSION['isLoggedIn'];
$user = $_SESSION['user'];

$actionURL = null;

if ($logged) {
    $action = "logout";
}
else {
    $action = "login";
}

echo json_encode(array(
    'user'=>$user,
    'isLoggedIn'=> $logged,
    'action' =>  $action
));
?>
