<?php
function get_products() {
    $fh_products = fopen('../data/productos.csv', 'r');

    $products = array();

    fgetcsv($fh_products, 0,',');

    while(($row = fgetcsv($fh_products, 0, ',')) !== FALSE) {
        $cols = count($row);

        array_push(
            $products,
            array(
                'id' => $row[0],
                'nombre' => $row[1],
                'descripcion' => $row[2],
                'precio' => $row[3],
            )
        );

    }

    return $products;
}

function add_product($data){

    $user = $_SESSION['user'];
    $prod_id = $data['id'];
    
    if(isset($_SESSION[$user][$prod_id])){
        $_SESSION[$user][$prod_id]++;
    }
    else{
        $_SESSION[$user][$prod_id]=1;
    }

    return $_SESSION[$user];
}

function remove_product($data){

    $user = $_SESSION['user'];
    $prod_id = $data['id'];
    
    if($_SESSION[$user][$prod_id] > 0){
        $_SESSION[$user][$prod_id]--;
    }
    else {
        unset($_SESSION[$user][$prod_id]);
    }

    return $_SESSION[$user];
}

function get_cart() {
    $user = $_SESSION['user'];
    $cart = $_SESSION[$user];

    $products = get_products();

    $cart_show = array();
        
    foreach ($cart as $id => $cant) {
        foreach ($products as $p){
            if($id == $p['id']) {
                $cart_show[] = array(
                    'id'=> $id,
                    'cant' => $cant,
                    'nombre' => $p['nombre'],
                    'precio' => $p['precio']
                );
            }
        }
    }
	
    return $cart_show;

}

?>
