<?php
session_start();

function logout(){
    $_SESSION['isLoggedIn'] = false;
}

function build_json($row){

    return [
        'user' => $row[0],
        'pass' => $row[1]
    ];

}

function check_user($user, $pass){
    // csv file with the users table
    if( ($fp = fopen('../data/usuarios.csv','r')) ){
        // Skip headers row
        fgetcsv($fp);
        while(($row=fgetcsv($fp)) !=false){

            $userRow =  build_json($row);
            $rowCSV[] = $userRow;

        }
    }

    foreach ($rowCSV as $row) {
        if (($row['user'] == $user) && ($row['pass'] == $pass)) {
            return true;
        }
    }

    return false;
}

if ($_SESSION['isLoggedIn']){
    logout();
}
else {
    if(check_user($_POST['user'], $_POST['pass'])){

        $user = $_POST['user'];

        $_SESSION['user'] = $_POST['user'];
        if (isset($_SESSION[$user])) {
        }
        else {
            $_SESSION[$user] = array();
        }

        $_SESSION['isLoggedIn'] = true;

        echo json_encode(
            array(
                'user'=> $_SESSION['user'],
                'isLoggedIn' => $_SESSION['isLoggedIn'],
                'data' => $_SESSION[$user],
            )
        );

    }
    else {
        echo json_encode(
            array(
                'isLoggedIn' => false
            )
        );
    }

}

?>

