$(document).ready(()=>{
    init()

    $('#loginForm').submit((event)=>{
        const user = $('input[name=user]').val()
        const pass = $('input[name=pass]').val()

        $.ajax({
            url:'/api/login.php',
            method:'POST',
            data:{
                user:user,
                pass:pass
            }
        }).
            done((data)=>{
                // We got successfull results

                response = JSON.parse(data)

                if (response.isLoggedIn){
                    location.href ='/'
                }
                else {
                    $('.wrong-credentials').attr({hidden:null})
                    $('input').val('')
                }
            }).
            fail((error)=>{
                // We failed
            })

        event.preventDefault()
    })
})

const init = () => {
    $.ajax('/api/index.php').
        done(()=>{
            console.log('done')
        }).
        fail(()=>{
            console.log('fail')
        })
}
