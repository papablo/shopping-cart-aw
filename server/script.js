$(document).ready(function(){
    init()


    $('.login-action').click(()=>{
        $.ajax('/api/login.php').done((data)=>{
            location.reload()
        });
    })

    $('#popoverCart').popover({
        html:true,
        trigger:'focus',
        content:() => {

            return $('#cartProducts').html()
        }
    })

    refreshCart(refreshCartPopover)

    getProducts((opt)=>{
        let template = Handlebars.compile($('#products_template').html())

        $('#listProducts').html(template({products:opt.products}))

        $('.add-to-cart-btn').click((event)=>{
            const data = event.target.dataset

            addToCart(data,refreshCartPopover)
        })

        $('.remove-from-cart-btn').click((event)=>{
            const data = event.target.dataset

            removeFromCart(data,refreshCartPopover)
        })
    })

    getCurrentUser(function(opt){
        if (opt.user.isLoggedIn) {
            $('.login').attr({hidden:true})
            $('.logout').attr({hidden:null})
        } else {
            // Show login and hide logout
            $('.login').attr({hidden:null})
            $('.logout').attr({hidden:true})

            // Hide buttons to load cart
            $('.add-to-cart-btn').hide()
            $('.remove-from-cart-btn').hide()

            // Hide button to show cart
            $('#showCartBtn').hide()
        }

        if(!(opt.user.isLoggedIn)){
        }

    })

})

const refreshCartPopover = (data) => {
    const cart = JSON.parse(data).filter((elem) => elem.cant > 0)
    const template = Handlebars.compile($('#popover_products_template').html())

    $('#cartProducts').html(template({cart:cart}))
    const total = cart.reduce((x,y)=>{
        return {cant:x.cant+y.cant}
    },{cant:0}) 

    $('.prod-quant').html(total.cant)

    if (total.cant < 1){
        $('#checkoutBtn').hide()
    }
    else {
        $('#checkoutBtn').show()
    }
}
