$(document).ready(function(){
    init()

    $('.login-action').click(()=>{
        $.ajax('/api/login.php').done((data)=>{
            location.href='/'
        });
    })

    getCurrentUser(function(opt){
        if (opt.user.isLoggedIn) {
            $('.login').attr({hidden:true})
            $('.logout').attr({hidden:null})
        } else {
            // Show login and hide logout
            $('.login').attr({hidden:null})
            $('.logout').attr({hidden:true})
        }

    })

    refreshCart((data) => {
        const cart = JSON.parse(data).filter((el) => el.cant>0).map((el) => {
            el.precio = parseInt(el.precio)
            return el
        })

        let template = Handlebars.compile($('#cart_table_template').html())

        let total = cart.reduce((x,y) => { return x + (y.cant*y.precio)},0 )

        $('#cartTable').html(template({products:cart, total:total}))

    })

    $('#confirmarBtn').click((event) => { 
        refreshCart((data) => {
            const cart = JSON.parse(data).filter((el) => el.cant>0).map((el) => {
                el.precio = parseInt(el.precio)
                return el
            })

            let total = cart.reduce((x,y) => { return x + (y.cant*y.precio)},0 )

            let products = cart.map((el) => el.id)

            const purchase = {
                productos:products.join(','),
                total:total
            }

            $.ajax({
                url:'/api/purchase.php',
                data:purchase,
                method:'post'
            }).
                done((data) => {
                    location.href = '/'
                })

        })
    })

})

