const removeFromCart = (data, callback) => {
    $.ajax({
        url:'/api/remove_product.php',
        data:data,
        method:'POST'
    }).done((data)=>{
            refreshCart(callback)
        })
}

const addToCart = (data,callback) => {

    $.ajax({
        url:'/api/add_product.php',
        data:data,
        method:'POST'
    }).done((data)=>{
            refreshCart(callback)
        })
    
}

const refreshCart = (callback) => {
    $.ajax('/api/get_cart.php').
        done((data)=>{
            callback(data)
        })
}

const init = () => {
    $.ajax('api/index.php').
        done(()=>{
            console.log('done')
        }).
        fail(()=>{
            console.log('fail')
        })
}

const getCurrentUser = (callback) => {
    $.ajax('api/current_user.php').
        done((data)=>{
            const user = JSON.parse(data)

            callback({user:user})
        })
}

const getProducts = (callback) => {

    $.ajax('/api/get_products.php').
        done((data) => {
            const products = JSON.parse(data)

            callback({products:products})
        })
}
